package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        String questType = this.guild.getQuestType();

        //Agile hanya dapat mengerjakan Delivery dan Rumble
        if (questType.equals("D")||questType.equals("R")){
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
