package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
    }

    @Override
    public void update() {
        String questType = this.guild.getQuestType();

        //Tidak usah menggunakan if karena knight dapat mengerjakan semua quest
        this.getQuests().add(this.guild.getQuest());
    }
}
