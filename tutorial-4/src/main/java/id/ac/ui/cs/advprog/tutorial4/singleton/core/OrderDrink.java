package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import java.lang.Thread;

public class OrderDrink {

    private String drink;

    //Perbedaan lazy dan eager
    private static OrderDrink orderDrink = null;

    private OrderDrink() {
        this.drink = "No order for Drink";

        try{
            System.out.println("Creating.....");
            Thread.sleep(2000);
            System.out.println("Done.....");
        } catch(InterruptedException e){
            e.printStackTrace();
        }

    }

    //get lazy Instance of OrderDrink
    public static OrderDrink getInstance() {
        if (orderDrink == null) {
            orderDrink = new OrderDrink();
        }
        return orderDrink;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public String toString() {
        return drink;
    }
}
