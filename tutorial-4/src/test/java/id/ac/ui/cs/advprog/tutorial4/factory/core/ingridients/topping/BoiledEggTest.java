package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class BoiledEggTest {
    private Class<?> beefClass;
    private BoiledEgg beef;

    @BeforeEach
    public void setUp() throws Exception {
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        beef = new BoiledEgg();
    }

    @Test
    public void testBoiledEggIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(beefClass.getModifiers()));
    }

    @Test
    public void testBoiledEggIsATopping() {
        Collection<Type> interfaces = Arrays.asList(beefClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testBoiledEggOverrideGetDescription() throws Exception {
        Method getDescription = beefClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testBoiledEggGetDescriptionReturnCorrectly() {
        String name = beef.getDescription();
        assertEquals(name, "Adding Guahuan Boiled Egg Topping");
    }
}

