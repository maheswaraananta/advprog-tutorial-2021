package id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiClass;
    private SnevnezhaShiratakiFactory snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.SnevnezhaShiratakiFactory");
        snevnezhaShirataki = new SnevnezhaShiratakiFactory();
    }

    //Menguji Class
    @Test
    public void testSnevnezhaShiratakiFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAMainFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.MainFactory")));
    }

    //Menguji Method
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateNoodle() throws Exception {
        Method getDescription = snevnezhaShiratakiClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateMeat() throws Exception {
        Method getDescription = snevnezhaShiratakiClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateTopping() throws Exception {
        Method getDescription = snevnezhaShiratakiClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateFlavor() throws Exception {
        Method getDescription = snevnezhaShiratakiClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateNoodleReturnCorrectly() throws Exception {
        Noodle noodle = snevnezhaShirataki.createNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Snevnezha Shirataki Noodles...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateMeatReturnCorrectly() throws Exception {
        Meat meat = snevnezhaShirataki.createMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Zhangyun Salmon Fish Meat...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateToppingReturnCorrectly() throws Exception {
        Topping topping = snevnezhaShirataki.createTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Xinqin Flower Topping...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateFlavorReturnCorrectly() throws Exception {
        Flavor flavor = snevnezhaShirataki.createFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding WanPlus Specialty MSG flavoring...");
    }
}
