package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Class<?> menuClass;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        menu = new Menu("RestaurantMenu", new InuzumaRamenFactory());
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuGetNameReturnCorrectly() {
        String name = menu.getName();
        assertEquals(name, "RestaurantMenu");
    }

    @Test
    public void testMenuGetNoodleReturnCorrectly(){
        Noodle noodle = menu.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testMenuGetMeatReturnCorrectly() {
        Meat meat = menu.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testMenuGetToppingReturnCorrectly() {
        Topping topping = menu.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testMenuGetFlavorReturnCorrectly() {
        Flavor flavor = menu.getFlavor();
        assertNotNull(flavor);
    }

}
