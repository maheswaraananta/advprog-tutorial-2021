package id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MainFactoryTest {
    private Class<?> mainFactoryInterface;

    @BeforeEach
    public void setup() throws Exception {
        mainFactoryInterface = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.MainFactory");
    }

    //Menguji Class sebagai abstract class
    @Test
    public void testMainFactoryIsAPublicInterface() {
        int classModifiers = mainFactoryInterface.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    //Menguji Method sebagai abstract method
    @Test
    public void testMainFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method getDescription = mainFactoryInterface.getDeclaredMethod("createFlavor");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMainFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method getDescription = mainFactoryInterface.getDeclaredMethod("createMeat");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMainFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method getDescription = mainFactoryInterface.getDeclaredMethod("createTopping");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMainFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method getDescription = mainFactoryInterface.getDeclaredMethod("createNoodle");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}
