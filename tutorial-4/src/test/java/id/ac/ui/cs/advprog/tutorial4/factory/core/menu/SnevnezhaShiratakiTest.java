package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("RestaurantSnevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiGetNameReturnCorrectly() {
        String name = snevnezhaShirataki.getName();
        assertEquals(name, "RestaurantSnevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodleReturnCorrectly() {
        Noodle noodle = snevnezhaShirataki.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testSnevnezhaShiratakiGetMeatReturnCorrectly() {
        Meat meat = snevnezhaShirataki.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testSnevnezhaShiratakiGetToppingReturnCorrectly() {
        Topping topping = snevnezhaShirataki.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavorReturnCorrectly(){
        Flavor flavor = snevnezhaShirataki.getFlavor();
        assertNotNull(flavor);
    }

}
