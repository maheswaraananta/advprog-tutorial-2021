package id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonClass;
    private MondoUdonFactory mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.MondoUdonFactory");
        mondoUdon = new MondoUdonFactory();
    }

    //Menguji Class
    @Test
    public void testMondoUdonFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAMainFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.noodlefactories.MainFactory")));
    }

    //Menguji Method
    @Test
    public void testMondoUdonFactoryOverrideCreateNoodle() throws Exception {
        Method getDescription = mondoUdonClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateMeat() throws Exception {
        Method getDescription = mondoUdonClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateTopping() throws Exception {
        Method getDescription = mondoUdonClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateFlavor() throws Exception {
        Method getDescription = mondoUdonClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryCreateNoodleReturnCorrectly() throws Exception {
        Noodle noodle = mondoUdon.createNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Mondo Udon Noodles...");
    }

    @Test
    public void testMondoUdonFactoryCreateMeatReturnCorrectly() throws Exception {
        Meat meat = mondoUdon.createMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Wintervale Chicken Meat...");
    }

    @Test
    public void testMondoUdonFactoryCreateToppingReturnCorrectly() throws Exception {
        Topping topping = mondoUdon.createTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Shredded Cheese Topping...");
    }

    @Test
    public void testMondoUdonFactoryCreateFlavorReturnCorrectly() throws Exception {
        Flavor flavor = mondoUdon.createFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding a pinch of salt...");
    }
}
