package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("RestaurantMondoUdon");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonGetNameReturnCorrectly() {
        String name = mondoUdon.getName();
        assertEquals(name, "RestaurantMondoUdon");
    }

    @Test
    public void testMondoUdonGetNoodleReturnCorrectly()  {
        Noodle noodle = mondoUdon.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testMondoUdonGetMeatReturnCorrectly() {
        Meat meat = mondoUdon.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testMondoUdonGetToppingReturnCorrectly(){
        Topping topping = mondoUdon.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testMondoUdonGetFlavorReturnCorrectly()  {
        Flavor flavor = mondoUdon.getFlavor();
        assertNotNull(flavor);
    }

}
