package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("RestaurantLiyuanSoba");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaGetNameReturnCorrectly(){
        String name = liyuanSoba.getName();
        assertEquals(name, "RestaurantLiyuanSoba");
    }

    @Test
    public void testLiyuanSobaGetNoodleReturnCorrectly(){
        Noodle noodle = liyuanSoba.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testLiyuanSobaGetMeatReturnCorrectly() {
        Meat meat = liyuanSoba.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testLiyuanSobaGetToppingReturnCorrectly() {
        Topping topping = liyuanSoba.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testLiyuanSobaGetFlavorReturnCorrectly() {
        Flavor flavor = liyuanSoba.getFlavor();
        assertNotNull(flavor);
    }

}
