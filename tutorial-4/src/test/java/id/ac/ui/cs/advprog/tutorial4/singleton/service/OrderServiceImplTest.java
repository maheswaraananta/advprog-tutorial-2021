package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceImplTest {
    private OrderServiceImpl orderService;

    @Mock
    OrderFood orderFood;

    @Mock
    OrderDrink orderDrink;

    @BeforeEach
    public void setup() throws Exception {
        orderFood = OrderFood.getInstance();
        orderDrink = OrderDrink.getInstance();
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceGetFood() {
        orderService.orderAFood("A Food");
        assertEquals(orderService.getFood().toString(), "A Food");
    }


    @Test
    public void testOrderServiceGetDrink() {
        orderService.orderADrink("A Drink");
        assertEquals(orderService.getDrink().toString(), "A Drink");
    }
}
