package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import jdk.nashorn.internal.objects.annotations.Constructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)


    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    //Apabila weaponRepo sudah diisi oleh bows dan spellbook
    private boolean filledRepository = false;

    private void putSpellbookInRepository(){
        for(Spellbook spellbook:spellbookRepository.findAll()){
            //Ubah dari spellbook ke weapon
            SpellbookAdapter spellToWeapon = new SpellbookAdapter(spellbook);
            //masukkan ke weaponRepository
            weaponRepository.save(spellToWeapon);
        }
    }

    private void putBowInRepository(){
        for(Bow bow:bowRepository.findAll()){
            //Ubah dari bow ke weapon
            BowAdapter bowToWeapon = new BowAdapter(bow);
            //masukkan ke weaponRepository
            weaponRepository.save(bowToWeapon);
        }
    }

    // get the list of weapon repository value
    @Override
    public List<Weapon> findAll() {
        if(filledRepository == false){
            putBowInRepository();
            putSpellbookInRepository();
            filledRepository = true;
        }
        return weaponRepository.findAll();
    }

    // attack based on weaponName and attackType
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = this.weaponRepository.findByAlias(weaponName);
        if(attackType == 0) {
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (normal attack): " + weapon.normalAttack());
        }
        else{
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (charged attack): " + weapon.chargedAttack());
        }

        weaponRepository.save(weapon);
    }

    //Get All logs
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
