package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;


public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(this.isAimShot);
    }

    @Override
    public String chargedAttack() {
        if(this.isAimShot == false){
            this.isAimShot = true;
            return  "Entered aim shot mode";
        }
        //Else
        this.isAimShot=false;
        return "Exit aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}