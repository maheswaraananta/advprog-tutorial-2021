package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

//Adaptor dari Weapon ke spell
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean hasChargedAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.hasChargedAttack = true;
    }

    //Mengembalikan small spell
    @Override
    public String normalAttack() {
        this.hasChargedAttack = true;
        return spellbook.smallSpell();
    }

    //Charged attack tidak bisa berturut-turut
    @Override
    public String chargedAttack() {
        if(this.hasChargedAttack) {
            this.hasChargedAttack = false;
            return spellbook.largeSpell();
        }
        return "Cannot Use charged attack twice";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
