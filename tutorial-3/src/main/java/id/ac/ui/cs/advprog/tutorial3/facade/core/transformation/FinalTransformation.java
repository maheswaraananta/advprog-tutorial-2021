package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
/**
 * Kelas ini mengimplementasikan caesar cipher shift 5
 */
public class FinalTransformation {

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int shift;
        if(encode){
            shift = 5;
        }
        else{
            shift = -5;
        }
        //Penerapan cipher shift
        Codex codex = spell.getCodex();
        int codexLen = codex.getCharSize();
        char [] res = spell.getText().toCharArray();
        for (int i = 0; i < res.length; i++) {
            int newChar = codex.getIndex(res[i]) + shift;
            //apabila shift ke belakang dan index dibawah 0
            if(newChar<0) {
                newChar += codexLen;
            }
            else{
                newChar %= codexLen;
            }
            res[i] = codex.getChar(newChar);
        }
        return  new Spell(new String(res),codex);
    }
}
