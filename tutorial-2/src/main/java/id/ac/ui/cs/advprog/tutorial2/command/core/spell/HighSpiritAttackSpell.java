package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

//Import highspirit dari spirit
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritAttackSpell extends HighSpiritSpell {
    //Constructor
    public HighSpiritAttackSpell(HighSpirit spirit) {
        super(spirit);
    }
    // Cast High Spirit Attack Spell
    @Override
    public void cast() {
        spirit.attackStance();
    }


    @Override
    public String spellName() {
        return spirit.getRace() + ":Attack";
    }
}
