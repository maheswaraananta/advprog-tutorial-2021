package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    //Bikin arraylist yang berisi spell
    ArrayList<Spell> spells;

    //Constructor chainspell
    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    //Cast spell bakal ngeloop semua chainspell yang ditaro
    @Override
    public void cast() {
        for (Spell spell:this.spells
        ) {
            spell.cast();

        }
    }

    //Undo bakal ngeloop kebalik semua chainspell yang ditaro
    @Override
    public void undo() {
        for (int i = spells.size()-1; i >=0 ; i--) {
            Spell spell = spells.get(i);
            spell.undo();
        }

    }


    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
