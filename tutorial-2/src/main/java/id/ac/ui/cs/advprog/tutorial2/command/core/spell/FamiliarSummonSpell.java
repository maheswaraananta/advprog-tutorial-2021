package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

//Panggil familiar
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;


public class FamiliarSummonSpell extends FamiliarSpell {

    //constructor
    public FamiliarSummonSpell(Familiar familiar){
        super(familiar);
    }
    //cast summon
    @Override
    public void cast(){
        familiar.summon();
    }


    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }
}
