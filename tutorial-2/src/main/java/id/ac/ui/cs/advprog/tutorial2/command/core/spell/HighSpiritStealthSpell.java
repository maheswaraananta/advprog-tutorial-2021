package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

//Import highspirit dari spirit
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    //Constructor
    public HighSpiritStealthSpell(HighSpirit spirit) {
        super(spirit);
    }

    //stance cast
    @Override
    public void cast() {
        spirit.stealthStance();
    }

    @Override
    public String spellName() { return spirit.getRace() + ":Stealth"; }

}
