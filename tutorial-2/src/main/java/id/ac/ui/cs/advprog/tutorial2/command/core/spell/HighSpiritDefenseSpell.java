package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

//import highspirit dari spirit
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritDefenseSpell extends HighSpiritSpell {
    //Constructor
    public HighSpiritDefenseSpell(HighSpirit spirit) {
        super(spirit);
    }

    //cast defense
    @Override
    public void cast() {
        spirit.defenseStance();
    }


    @Override
    public String spellName() {
        return spirit.getRace() + ":Defense";
    }
}
