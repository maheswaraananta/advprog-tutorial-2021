package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

// Import highspirit from spirit
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritSealSpell extends HighSpiritSpell {
    // Constructor
    public HighSpiritSealSpell(HighSpirit spirit) {
        super(spirit);
    }

    // seal the highspirit
    @Override
    public void cast() {
        spirit.seal();
    }


    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
